#include <stdio.h>

 int first_array[] = {1,2,3};
 int second_array[] = {4,5,6};

int scalar_product(size_t pointer, int first_arr[],int second_arr[]){
	int sum = 0;
	while(pointer != -1) {
		sum = sum + first_arr[pointer] * second_arr[pointer];
		pointer = pointer - 1;
		}
	return sum;
}

int main(void){

size_t pointer = sizeof(first_array)/sizeof(first_array[0]) - 1;

printf("Scalar product is %d \n", scalar_product(pointer, first_array,second_array));
return 0;
}
